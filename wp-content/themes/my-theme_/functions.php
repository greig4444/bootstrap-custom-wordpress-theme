<?php
   /* Theme setup */
   add_action( 'after_setup_theme', 'wpt_setup' );
       if ( ! function_exists( 'wpt_setup' ) ):
           function wpt_setup() {  
               register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
           } endif;
   
           function wpt_register_js() {
               wp_register_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', 'jquery');
               wp_enqueue_script('jquery.bootstrap.min');
                wp_enqueue_script( 'my_custom_js', get_template_directory_uri() . '/assets/js/main.js');
           }
           add_action( 'init', 'wpt_register_js' );
   
           function wpt_register_css() {
               wp_register_style( 'bootstrap.min', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
               wp_enqueue_style( 'bootstrap.min' );
               wp_enqueue_style('style.css');
           }
           add_action( 'wp_enqueue_scripts', 'wpt_register_css' );
   
   
   // This function enqueues the Normalize.css for use. The first parameter is a name for the stylesheet, the second is the URL. Here we
   // use an online version of the css file.
   function add_bootstrap_CSS() {
       wp_enqueue_style( 'bootstrap-styles', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
   }
   
   // Register a new sidebar simply named 'sidebar'
   function add_widget_Support() {
                   register_sidebar( array(
                                   'name'          => 'Sidebar',
                                   'id'            => 'sidebar',
                                   'before_widget' => '<div>',
                                   'after_widget'  => '</div>',
                                   'before_title'  => '<h2>',
                                   'after_title'   => '</h2>',
                   ) );
   }
   // Hook the widget initiation and run our function
   add_action( 'widgets_init', 'add_Widget_Support' );
   
   // Register a new navigation menu
   function add_Main_Nav() {
     register_nav_menu('header-menu',__( 'Header Menu' ));
   }
   // Hook to the init action hook, run our navigation menu function
   add_action( 'init', 'add_Main_Nav' );
   
   ?>
<?php // Register custom navigation walker
   require_once('bootstrap-navwalker.php');
   ?>
<?php
   function wpb_hidetitle_class($classes) {
   if ( is_single() || is_page() ) : 
   $classes[] = 'hidetitle';
   return $classes;
   endif; 
   return $classes;
   }
   add_filter('post_class', 'wpb_hidetitle_class');
   ?>