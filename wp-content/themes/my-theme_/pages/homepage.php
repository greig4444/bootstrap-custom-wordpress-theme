<?php /* Template Name: homepage */ ?>
<?php get_header(); ?>
<main class="wrap">
   <section class="content-area content-thin">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <article class="article-full">
         <!-- <header>
            <h2><?php //the_title(); ?></h2>
            By: <?php //the_author(); ?>
            </header>-->
         <div class="container-fluid"> <!-- start of container --> 
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
               <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
               </ol>
               <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                     <img class="d-block img-fluid" src="http://www.greigmcmahon.com/wordpress/wp-content/uploads/2018/05/001.jpg" alt="First slide">
                     <div class="carousel-caption d-none d-md-block">
                        <h3>Title Here</h3>
                        <p>Text here</p>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img class="d-block img-fluid" src="http://www.greigmcmahon.com/wordpress/wp-content/uploads/2018/05/002.jpg" alt="Second slide">
                     <div class="carousel-caption d-none d-md-block">
                        <h3>Title Here</h3>
                        <p>Text here</p>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img class="d-block img-fluid" src="http://www.greigmcmahon.com/wordpress/wp-content/uploads/2018/05/003.jpg" alt="Third slide">
                     <div class="carousel-caption d-none d-md-block">
                        <h3>Title Here</h3>
                        <p>Text here</p>
                     </div>
                  </div>
               </div>
               <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
         </div>
         <!-- start of services -->
         <div class="main">
            <section id="services" class="module">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6 col-sm-offset-3">
                        <div class="module-subtitle font-serif"></div>
                     </div>
                  </div>
                  <div class="row multi-columns-row">
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Web Development</h3>
                           Web Development is the major factor on how the website will function on the internet and relies on skills like modern computer languages, script validation, database development, web services and internet technologies. Being an experienced qualified web developer I can guarantee the best possible functionality for your business website at an honest affordable price.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Web Design</h3>
                           Web design is about more than just creating and implementing a design and content. Correct methods such as readable fonts, layout, colours used, information architecture, site structure and navigation are essential and are implemented in to every website I build. Existing websites projects can also be modified and redesigned to suit a modern day standard.
                           &nbsp;
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Hosting</h3>
                           Choosing the right web hosting package that best suits your website can be confusing and costly. I offer cost-efficient hosting packages that best suit the websites requirements and take in to consideration factors like disk space, speed, bandwidth allotment. I also offer hosting packages to websites that I have not build.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Domain</h3>
                           It is essential that you choose the right domain name as it serves as the headline for your business and can be just as important as the business name or logo. There are many right and wrong methods in this process. Contact me to discuss what best domain name suits your business.
                        </div>
                     </div>
                  </div>
                  <div class="row multi-columns-row">
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Online Marketing</h3>
                           Online marketing is advertising and marketing methods that are used on the World Wide Web to drive direct sales to your business via such things as email campaigns to pay per click advertising, contact me to see what best campaign would suit your business.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Social Media Marketing</h3>
                           Social Media Marketing is the process of gaining website traffic to your website via Facebook, Twitter, Google+ etc. it’s the most used free marketing tool in the industry and I will use my proven experience for your business to get the most out this marketing tool, I can even build, professionalize and maintain your social media pages for you.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Email Marketing</h3>
                           Email marketing allows you to connect with your customer in a highly personalized way, while staying within your budget, while some marketing trends come and go, email remains the most powerful channel available to the modern market. I will build your marketing campaign using all modern day technologies that work on all devices and designed towards your target market.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Web Banners (HTML5)</h3>
                           A web banner is an advertisement displayed into a web page. Banner ads are intended generate traffic and customers to a website by linking to it. With the web platform flash all but absolute all banners will be built using HTML5, CSS &amp; JavaScript thus allowing your banner to work on all modern day devices.
                        </div>
                     </div>
                  </div>
                  <div class="row multi-columns-row">
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Search Engine Optimization</h3>
                           Search engine optimization is a must for any company that wants to keep ahead of their competition and will be added to any new dynamic+ websites I create for free, which will give your business the immediate online presence it deserves. SEO can also be added to any website I have not created. Contact me to find out what SEO package best suits your business.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Internet Security</h3>
                           Websites and web servers are unfortunately prone to security risks and keeping you’re and you’re clients information secure is a must. Website security will be added to all new dynamic+ websites and can also be added to existing websites. Monthly contract packages are available and are highly recommended for higher end websites such as eCommerce or event management.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">eCommerce</h3>
                           An eCommerce website will be built and designed to suit your target market with high end security and perfect functionality added. If you choose, training can be given so you can add and maintain your own product or if you don’t have the time I can do this for you.
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                           <div class="features-icon"></div>
                           <h3 class="features-title font-alt">Virus &amp; Spyware Removal</h3>
                           PC or Laptop running slow then you could be a victim of spyware, malware, Trojans and viruses. I offer a service were I will guarantee the removal of these viruses or in severe cases a full system restore at an affordable price.
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- end of services -->
         </div>
         </div>
            <!-- end of container -->

        <?php the_content(); ?>
       </article>
      <?php endwhile; else : ?>
      <article>
         <p>Sorry, no page was found!</p>
      </article>
      <?php endif; ?>
   </section>
   <!--<?php //get_sidebar(); ?> -->  <!-- Sidebar -->
</main>
<?php get_footer(); ?> <!-- Footer -->