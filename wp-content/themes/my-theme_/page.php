<?php
   /**
    * The template for displaying all pages
    *
    * This is the template that displays all pages by default.
    * Please note that this is the WordPress construct of pages
    * and that other 'pages' on your WordPress site may use a
    * different template.
    */
   
   get_header(); ?>
<main class="wrap">
   <section class="content-area content-thin">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <article class="article-full">
         <!-- <header>
            <h2><?php the_title(); ?></h2>
            By: <?php the_author(); ?>
            </header>-->
         <?php the_content(); ?>
      </article>
      <?php endwhile; else : ?>
      <article>
         <p>Sorry, no page was found!</p>
      </article>
      <?php endif; ?>
   </section>
   <?php get_sidebar(); ?>
</main>
<?php get_footer(); ?>